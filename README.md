# MPCDF Metadata Tool

```mermaid
graph TD;
  A[<font size=4><b>mmdFormats.py</b><br/><font size=2>Metadata implemented there]-->B[<font size=4><b>mmdCreate.py</b><br/><font size=2>Creation of a dataset with MMD metadata<br/>Attribution of an ID];
  B-->C[<font size=4><b>Output file</b><br/><font size=2>Default name: metadata.mmd];
  D[Input from terminal]-->B;
  E[Input from template file]-->B
```

# About the format

## Main fields

- title
- version
- description

## Properties

[Property Name]
- default
- description
- optional
- repeatable
- vocabulary (list)
- affiliations

# Mapping with the CKAN Scheming extension

- slugified([Property Name]): field_name
- [Property Name] : label
- description: help_text
- default: form_placeholder
- optional: !required
- vocabulary: choice.label
- slugified(vocabulary): choice.value
- affiliations: repeating_label, repeating_subfields, ...
