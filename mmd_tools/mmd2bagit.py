#!/usr/bin/env python3

import json
import argparse
import bagit

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--folder", help="The folder which should be turned into a bagit")
    parser.add_argument("-m", "--metadata", help="The file with the metadata in MMD format")
    args = parser.parse_args()

    try:
        infile = open(args.metadata, "r")
        data = json.load(infile)
        bag = bagit.make_bag(args.folder, data)
    except:
        print("Can't find folder or metadata file")

if __name__ == "__main__":
    main()
