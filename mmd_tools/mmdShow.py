#!/usr/bin/env python3

import json
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="specify input file")
    parser.add_argument("-o", "--outputformat", help="specify output format")
    args = parser.parse_args()

    try:
        infile = open(args.input, "r")
        data = json.load(infile)
      
        if args.outputformat == "html":
            print("<html>\n<head>\n</head>\n<body>\n")
            print("<h1>" + args.input + "</h1>")
            print("<table border='1'>")
            for key, value in data.items():
                print("<tr><td>" + key + "</td><td>" + str(value) + "</td></tr>\n")
            print("</table>")
            print("</body>\n</html>")
        else:
            print("Inputfile: ", args.input)
            for key, value in data.items():
                print(key + ": " + str(value))
    except Exception as e:
        #print("Can't find input file (specified via -i)")
        print(e)


if __name__ == "__main__":
    main()
