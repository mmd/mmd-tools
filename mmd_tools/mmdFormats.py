#!/usr/bin/env python3

import json
from datetime import date
import os
import pathlib


def convertJson2MetadataDict(mfile):
    with open(mfile, "r") as f:
        metadata_format_dict = json.load(f)

    output = {}
    output["title"] = metadata_format_dict["title"]
    output["version"] = metadata_format_dict["version"]
    output["description"] = metadata_format_dict["description"]
    output["properties"] = []

    for prop in metadata_format_dict["properties"]:
        entry = {}
        entry["name"] = prop["field_name"]
        if "description" in prop:
            entry["description"] = prop["description"]
        else:
            entry["description"] = ""
        if "default" in prop:
            default = prop["default"]
            if default == "getUsername":
                default = os.environ.get("USER")
            elif default == "todaysDate":
                default = str(date.today())
            entry["default"] = default
        else:
            entry["default"] = ""
        if "optional" in prop:
            entry["optional"] = prop["optional"]
        else:
            entry["optional"] = ""
        if "repeatable" in prop:
            entry["repeatable"] = prop["repeatable"]
        else:
            entry["repeatable"] = ""
        if "vocabulary" in prop:
            entry["vocabulary"] = prop["vocabulary"]
        else:
            entry["vocabulary"] = []
        if "subfields" in prop:
            entry["subfields"] = prop["subfields"]
        else:
            entry["subfields"] = []

        output["properties"].append(entry)

    return output


class MmdEntry:
    #    name = ""
    #    description = ""
    #    optional = True
    #    vocabulary = []
    #    repeatable = False
    #    value = ""
    #    subfields=[]

    def __init__(
        self, name, description, value, optional, vocabulary, repeatable, subfields
    ):
        self.name = name
        self.description = description
        self.optional = optional
        self.vocabulary = vocabulary
        self.value = value
        self.repeatable = repeatable
        self.subfields = subfields

    def toJson(self):
        jsonEntry = {}
        jsonEntry[self.name] = self.value
        return json.dumps(jsonEntry)


class MmdFormat:

    data = []
    id = ""
    mmdFormat = ""
    mmdVersion = ""
    mmdDescription = ""

    def __init__(self, metadata_file):
        metadata = convertJson2MetadataDict(metadata_file)

        self.data = []
        self.id = ""
        self.mmdFormat = metadata["title"]
        self.mmdVersion = metadata["version"]
        self.mmdDescription = metadata["description"]

        for property in metadata["properties"]:
            self.data.append(MmdEntry(*property.values()))

    @classmethod
    def from_format(cls, format):
        """Alternative constructor, takes format string

        Instantiates class with a path to the internal data files.
        """
        metadata_file = str(
            pathlib.Path(__file__).parent.resolve() / "formats" / f"{format}.json"
        )
        return cls(metadata_file)

    def toJson(self):
        data = {}
        data["id"] = self.id
        data["mmdFormat"] = self.mmdFormat
        data["mmdVersion"] = self.mmdVersion
        print("DATA: ", data)

        for entry in self.data:
            entry_dict = json.loads(entry.toJson())
            key = list(entry_dict.keys())[0]
            data[key] = entry_dict[key]

        dataJson = json.dumps(data, indent=4, ensure_ascii=False)
        return dataJson


def get_list_of_formats():
    """Return list of formats that can be chosen

    The list is generated from the json files in the formats folder"""
    format_dir = pathlib.Path(__file__).parent / "formats"
    return [file.name[:-5] for file in format_dir.glob("*.json")]
