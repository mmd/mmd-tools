#!/usr/bin/env python3

from datetime import date
import uuid
import argparse
import json

from . import mmdFormats
import pathlib

from iterfzf import iterfzf


def prompt_with_help(prompt_text, description, extended_help):
    """
    Create a prompt with a customized prompt text. If the user press '?', an extended help is displayed with a description of the field.
    Arguments:
        prompt_text: a string
        description: a string
        extended_help: a string
    Returns:
        record: a string corresponding to the input from the user (other than '?')
    """

    record = input(prompt_text)
    while record == "?":
        print("  Field description: ", description)
        print("  Help: ", extended_help)
        record = input(prompt_text)
    return record


def prompt_from_properties(entry):
    """
    - Create a customized prompt text adapted to the type of metadata field (i.e. how its properties are defined). We consider three types of fields: simple text (potentially repeatable), multiple choice and affiliations.
    - Set the value of the entry according to the input of the user.
    Arguments:
        entry: an object of Type mmdFormats.MmdEntry.
    """

    record = ""
    prompt_text = entry.name + ": "

    # Simple text (potentially repeatable): entry.subfields and entry.vocabulary set to False

    if entry.value:
        if isinstance(entry.value, str):
            default = entry.value
            prompt_text += " [" + default + "] "
    else:
        default = ""

    if not entry.subfields and not entry.vocabulary:
        extended_help = "Type value and press Enter. If you don't type anything, the default value is taken."
        record = prompt_with_help(prompt_text, entry.description, extended_help)

        if entry.repeatable:
            inputs = [record]
            while record != "":
                record = input(prompt_text)
                if record != "":
                    inputs.append(record)
                    record = inputs

    # Multiple choice: entry.vocabulary set to True, entry.repeatable and entry.subfields set to False

    if entry.vocabulary:
        voc = entry.vocabulary
        prompt_text += " (Type ! to select an item)"
        extended_help = "Type ! to access the selection list. Choose an item with the up and down arrows and press Tab to select it. Repeat if multiple selection is allowed. Press Enter when done."
        record = prompt_with_help(prompt_text, entry.description, extended_help)
    else:
        voc = None

    if voc != None and record == "!":
        record = iterfzf(iter_voc(voc))

    # Subfields: entry.subfields set to True

    if entry.subfields:
        prompt_text += " (Fill in the value for every subfield)  "
        extended_help = "This field has subfields. For each field, you can type '?' to get a description."
        prompt_with_help(prompt_text, entry.description,extended_help)

        record={}
        for subfield in entry.subfields:
            prompt_text_subfield="    "+subfield["label"]
            if "vocabulary" not in subfield:
                extended_help = "Type value and press Enter. If you don't type anything, the default value is taken."
                record[subfield["field_name"]]=prompt_with_help(prompt_text_subfield,subfield.get("description",""),extended_help)
            else:
                prompt_text_subfield += " (Type ! to select an item)"
                extended_help = "Type ! to access the selection list. Choose an item with the up and down arrows and press Tab to select it. Repeat if multiple selection is allowed. Press Enter when done."

                record[subfield["field_name"]]=prompt_with_help(prompt_text_subfield,subfield.get("description",""),extended_help)
            
                if subfield["vocabulary"] != None and record[subfield["field_name"]] == "!":
                    record[subfield["field_name"]] = iterfzf(iter_voc(subfield["vocabulary"]))
    

#        # If a template is given, the user will be given the opportunity to change the subfields values for each entry.
#        if entry.value:
#            for subfields_dict in entry.value:
#                for fieldname,fieldvalue in subfields_dict.items():
#                name = subfields_dict["label"]
#                affiliations = affiliations_dict["affiliations"]
#                name_aff = (
#                    " [Name: "
#                    + name
#                    + "; Affiliations: "
#                    + ",".join(affiliations)
#                    + "] "
#                )
#
#                record2 = prompt_with_help(
#                    prompt_text + name_aff, entry.description, extended_help
#                )
#                creator = {}
#                if record2 == "":
#                    creator["name"] = name
#                    creator["affiliations"] = affiliations
#                else:
#                    creator["name"] = record2
#                    creator["affiliations"] = iterfzf(
#                        iter_voc(entry.affiliations), multi=True
#                    )
#                inputs.append(creator)
#
#        # Here the user can add new entries
#        record = prompt_with_help(prompt_text, entry.description, extended_help)
#        while record != "":
#            creator = {}
#            creator["name"] = record
#            creator["affiliations"] = iterfzf(iter_voc(entry.affiliations), multi=True)
#            inputs.append(creator)
#            record = prompt_with_help(prompt_text, entry.description, extended_help)
#        record = inputs

    if record != "":
        entry.value = record


def iter_voc(voc):
    for v in voc:
        yield v
        # sleep(0.01)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o", "--outputfile", help="specify output file", default="metadata.mmd"
    )
    parser.add_argument(
        "-f",
        "--format",
        help="specify metadata format",
        default="mmd",
        choices=mmdFormats.get_list_of_formats(),
    )
    parser.add_argument(
        "-t", "--template", help="A JSON file with some pre-filled key value pairs"
    )
    parser.add_argument(
        "-m",
        "--mmdfile",
        help="An MMD file to be updated. This is different from the pre-filling with key-value pairs, because the id of the metadata is preserved.",
    )

    args = parser.parse_args()

    print(args)

    outfile = "metadata.mmd"

    # The outfile which will store the metadata:
    if args.outputfile:
        outfile = args.outputfile
    print("Outputfile: ", args.outputfile)

    # Was there a metadataformat specified. If not, use MmdSimple:
    print("Metadata format: ", args.format)
    metadata = mmdFormats.MmdFormat.from_format(args.format)

    # Was there a template with JSON encoded key value pairs specified:
    if args.template:
        infile = open(args.template)
        kvps = json.load(infile)
        for entry in metadata.data:
            if entry.name in kvps:
                entry.value = kvps[entry.name]

    # Is an MMD file to be modified?
    if args.mmdfile:
        infile = open(args.mmdfile)
        kvps = json.load(infile)
        metadata.id = kvps["id"]
        for entry in metadata.data:
            if entry.name in kvps:
                entry.value = kvps[entry.name]
    else:
        # If not, create a new UUID as id:
        metadata.id = str(uuid.uuid4())

    #######################################################
    # Part where the user set the metadata.
    # For each field, the prompt has different components according to which property is activated.
    # It follows this general pattern:
    # <Property Name>: [<Default>] <Field Type>
    #######################################################

    print('Fill in each field. Type "?" for a description of the field and for help.')
    print("___________________________________________")

    for entry in metadata.data:
        prompt_from_properties(entry)

    # Writing back the metadata:
    with open(outfile, "w", encoding="utf-8") as outputfile:
        outputfile.write(metadata.toJson())

    print("Your metadata was written to ", outfile)

if __name__ == "__main__":
    main()
