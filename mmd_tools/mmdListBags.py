#!/usr/bin/env python3

import argparse
import bagit
import pathlib
import sys


def main():
    parser = argparse.ArgumentParser(
        description="Find and display all bagit files below a certain folder.")
    parser.add_argument("folder",
                        help="The folder under which bags are searched.")
    parser.add_argument("-o", "--outputfile", help="Output file")
    parser.add_argument("-s", "--showempty", help="Show empty values",
                        action='store_true')
    args = parser.parse_args()

    if args.outputfile is None:
        outputfile = sys.stdout
    else:
        outputfile = open(args.outputfile, "w")

    try:
        folder = pathlib.Path(args.folder)
        file_list = folder.glob("**/bag-info.txt")

        for file in file_list:
            bag = bagit.Bag(str(file.parent))
            outputfile.write("Folder: {} \n".format(str(file.parent)))
            for k, v in bag.info.items():
                if v != "" or args.showempty:
                    outputfile.write(f"  {k}: {v}\n")
            outputfile.write("\n")
    finally:
        outputfile.close()


if __name__ == "__main__":
    main()
