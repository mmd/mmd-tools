#!/usr/bin/env python3

import json
import xmltodict
import requests
from nested_lookup import nested_lookup
import re
import argparse

#List of files for supported Metadata Formats
formats_list=["https://gitlab.mpcdf.mpg.de/api/v4/projects/8112/repository/files/mmd.json/raw","https://gitlab.mpcdf.mpg.de/api/v4/projects/8112/repository/files/dublinCore.json/raw","https://gitlab.mpcdf.mpg.de/api/v4/projects/8112/repository/files/datacite.json/raw","https://schema.datacite.org/meta/kernel-4.4/metadata.xsd"]

def convertXSD2Dict(content, title, version, description):
    metadata_format_dict = xmltodict.parse(content)

    output={}
    output["title"]=title
    output["version"]=version
    output["description"]=description
    output["properties"]=[]

    for e in metadata_format_dict['xs:schema']['xs:element']['xs:complexType']['xs:all']['xs:element']:
        entry={}
        name=e['@name']
        entry['name']=name
        temp = name[0].upper() + name[1:]
        temp = re.findall('[A-Z][^A-Z]*', temp)
        human_name = " ".join(temp)
        documentation = nested_lookup('xs:documentation',e)[0]
        if isinstance(documentation,list):
            description=' '.join(documentation)
        else:
            description=documentation
        entry['description']=human_name+' - '+description
        entry['default']=""
        entry['optional']=True
        entry['vocabulary']=[]
        output["properties"].append(entry)

    return output

def loadMetadataFormat(metadata_format_file, title, version, description,fileName):
    response=requests.get(metadata_format_file)
    content=response.text
    fileName='../'+fileName
    if 'json' in metadata_format_file:
        with open(fileName,'w') as f:
            f.write(content)
    else:
        fileName=fileName.replace('.xsd','.json')
        metadata=convertXSD2Dict(content,title,version,description)
        with open(fileName,'w') as f:
            json.dump(metadata,f,indent=4)

def main():
    print("Specify metadata format file.")
    integers_list=[]
    for i,f in enumerate(formats_list):
        print(str(i)+" - "+f)
        integers_list.append(str(i))

    user_input=input("Format file ["+"/".join(integers_list)+"] :" )
    chosen_file=formats_list[int(user_input)]
    print("Metadata file to load: ",chosen_file)
    title=input("Title? ")
    version=input("Version? ")
    description=input("Description? ")
    for s in chosen_file.split('/'):
        if ".json" in s:
            fileName=s
    newFileName=input("Rename file? ["+fileName+"]")
    if newFileName!="":
        fileName=newFileName
    loadMetadataFormat(chosen_file,title,version,description,fileName)


if __name__ == "__main__":
    main()
